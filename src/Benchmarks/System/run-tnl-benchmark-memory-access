#!/bin/bash

# exit as soon as there is an error
set -o errexit
# abort when an unset variable is used
set -o nounset


log_file_base="tnl-benchmark-memory-access"
threads="1 2 4 8"
access_types="sequential random"
element_sizes="1 2 4 16 64 256"
#max_array_size="33554432"   # 32 MB
#max_array_size="67108864"   # 64 MB
#max_array_size="134217728"  # 128 MB
max_array_size="268435456"  # 256 MB
#max_array_size="536870912"  # 512 MB
#max_array_size="1073741824" # 1 GB

if [[ -f "${log_file_base}.log" ]]; then
   echo "Deleting exisiting log file ${log_file_base}.log..."
   rm "${log_file_base}.log"
fi

for threads_num in ${threads}
do
   log_file="${log_file_base}-${threads_num}-threads.log"
   if [[ -f "${log_file}" ]]; then
      echo "File ${log_file} already exixts - skipping."
      continue
   fi
   for access_type in ${access_types}
   do
      for element_size in ${element_sizes}
      do
         tnl-benchmark-memory-access --threads-count ${threads_num}      \
                                     --access-type ${access_type}        \
                                     --element-size ${element_size}      \
                                     --max-array-size ${max_array_size}  \
                                     --log-file ${log_file}              \
                                     --output-mode append

         tnl-benchmark-memory-access --threads-count ${threads_num}      \
                                     --access-type ${access_type}        \
                                     --element-size ${element_size}      \
                                     --write-test yes                    \
                                     --read-test no                      \
                                     --max-array-size ${max_array_size}  \
                                     --log-file ${log_file}              \
                                     --output-mode append

         if [[ "$access_type" == "sequential" ]]; then
            tnl-benchmark-memory-access --threads-count ${threads_num}     \
                                        --access-type ${access_type}       \
                                        --element-size ${element_size}     \
                                        --interleaving yes                 \
                                        --max-array-size ${max_array_size} \
                                        --log-file ${log_file}             \
                                        --output-mode append

            tnl-benchmark-memory-access --threads-count ${threads_num}     \
                                        --access-type ${access_type}       \
                                        --element-size ${element_size}     \
                                        --interleaving yes                 \
                                        --write-test yes                   \
                                        --read-test no                     \
                                        --max-array-size ${max_array_size} \
                                        --log-file ${log_file}             \
                                        --output-mode append
         fi
      done
   done
done

# Merge log files
echo "Merging log files"
for threads_num in ${threads}
do
   log_file="${log_file_base}-${threads_num}-threads.log"
   cat $log_file >> ${log_file_base}.log
done
